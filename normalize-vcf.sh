#!/bin/bash
#USAGE ➜ normalize-vcf.sh <BCFTOOLS-BINARY> <VCF-FILE.VCF> <GENOME.FA> vcf_hg19_chr_contig.txt

bcftools=$1
vcf=${2%".vcf"}
genome_fasta=$3
header_lines=$4

cat <($bcftools view -h $vcf.vcf | grep ^##) \
    <(cat $header_lines) \
    <(bcftools view -h $vcf.vcf | grep -v ^##) > $vcf.header

$bcftools reheader -h $vcf.header $vcf.vcf -o $vcf.reheader.vcf
$bcftools view $vcf.reheader.vcf -Oz -o $vcf.reheader.vcf.gz
$bcftools norm -f $genome_fasta $vcf.reheader.vcf.gz -O z > $vcf.reheader.leftnorm.vcf.gz

rm $vcf.header $vcf.reheader.vcf
