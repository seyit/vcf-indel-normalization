# vcf-indel-normalization

Indels descriptions are generally problematic in VCF, because different "variant callers" handle them differently. There is no consensus yet on how to describe an indel. However, it is highly important to describe indels properly before the annotation process.

Left-normalization is a method to standardize an indel. Left-normalization means shifting the start position of that variant to the left while keeping the same alleles length till it is no longer possible to do so. 

When annotating with Annovar or wAnnovar, left-normalization increases the likelihood of any variant will be correctly annotated.

![image.png](./image.png)

Further information: 
- http://annovar.openbioinformatics.org/en/latest/articles/VCF/
- https://genome.sph.umich.edu/wiki/Variant_Normalization

Here I wrote a small bash script to left-normalize vcf files using Bcftools.

Dependencies:
- Bcftools
- Genome Fasta

**USAGE**

`normalize-vcf.sh <BCFTOOLS-BINARY> <VCF-FILE.VCF> <GENOME.FA> vcf_hg19_chr_contig.txt`
